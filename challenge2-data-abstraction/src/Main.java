import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer value1, value2, calculatorOperation;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Masukkan Value 1:");
        value1 = keyboard.nextInt();
        System.out.println();

        System.out.print("Masukkan Value 2:");
        value2 = keyboard.nextInt();
        System.out.println();

        System.out.println("Please enter calculation operation:");
        System.out.println("1. Add Value");
        System.out.println("2. Sub Value");
        System.out.println("3. Multiply Value");
        System.out.println("4. Divide Value");
        System.out.print("Pilihan Anda:");
        calculatorOperation = keyboard.nextInt();
        System.out.println();

        switch (calculatorOperation) {
            case 1:
                CalculatorOperation addValue = new CalculatorOperation(value1, value2);
                System.out.printf("Pilihan Anda: %d", addValue.addValue());
                break;
            case 2:
                CalculatorOperation subValue = new CalculatorOperation(value1, value2);
                System.out.printf("Pilihan Anda: %d", subValue.divideValue());
                break;
            case 3:
                CalculatorOperation multiplyValue = new CalculatorOperation(value1, value2);
                System.out.printf("Pilihan Anda: %d", multiplyValue.multiplyValue());
                break;
            case 4:
                CalculatorOperation divide = new CalculatorOperation(value1, value2);
                System.out.printf("Pilihan Anda: %f", divide.divideValue());
                break;
        }

    }
}