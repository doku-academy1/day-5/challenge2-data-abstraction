public interface Operation {
    Integer addValue();
    Integer subValue();
    Integer multiplyValue();
    Float divideValue();
}
