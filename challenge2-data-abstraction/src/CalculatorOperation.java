public class CalculatorOperation implements Operation {
    Integer value1, value2;

    public CalculatorOperation(Integer value1, Integer value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public Integer getValue1() {
        return value1;
    }

    public Integer getValue2() {
        return value2;
    }

    public Float getValue1Float() {
        float v1 = value1;
        return v1;
    }

    public Float getValue2Float() {
        float v2 = value2;
        return v2;
    }

    @Override
    public Integer addValue() {
        return getValue1()+getValue2();
    }

    @Override
    public Integer subValue() {
        return getValue1()-getValue2();
    }

    @Override
    public Integer multiplyValue() {
        return getValue1()*getValue2();
    }

    @Override
    public Float divideValue() {
        return getValue1Float()/getValue2Float();
    }
}
